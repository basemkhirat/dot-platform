<?php

return [

    "general_options" => "General Options",

    "dot_version" => "Dot platform version",
    "check_for_update" => "Check for updates",
    "checking" => "Checking..",
    "how_update" => "How to update",
    "version_available" => "Dot Platform <strong style='font-family: sans-serif, Arial, Verdana'>:version</strong> is available!",
    "up_to_date" => "Dot platform is up to date.",

    'attributes' => [
        'site_name' => 'Site name',
        'site_slogan' => 'Site slogan',
        'site_email' => 'Site email',
        'site_copyrights' => 'Site copyrights',
        'timezone' => 'Time zone',
        'date_format' => 'Date format',
        'locale' => 'Locale',
        'site_status' => 'Site status',
        'offline_message' => 'Offline message'
    ],

    "permissions" => [
        "general" => "Manage general options"
    ]
];
