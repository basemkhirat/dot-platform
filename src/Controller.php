<?php

namespace Dot\Platform;

use Illuminate\Routing\Controller as LaravelController;

/*
 * Class Controller
 * @package Dot\Platform
 */
class Controller extends LaravelController
{

    /*
     * Controller constructor.
     */
    function __construct()
    {
        // Write the awesome..
    }

}
